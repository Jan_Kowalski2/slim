<?php

    namespace Interfaces;

    interface ModelInterface {

        public function __construct($queryBuilder);
        public function init();
        public function index();
        public function getKeys();

    }