<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/', \Blog\Controllers\PostController::class . ':index');
$app->get('/posts/{id}', \Blog\Controllers\PostController::class . ':show');

