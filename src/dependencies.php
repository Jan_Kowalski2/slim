<?php

$container = $app->getContainer();

$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

$container['database'] = function ($c) {
    $db = $c->get('settings')['database'];
    try {

        return new PDO('sqlite:' . $db['file'], null, null, $db['options']);
    } catch(PDOException $e) {
        die($e->getMessage());
    }
};

$container['queryBuilder'] = new Blog\Database\QueryBuilder($container->get('database'));

$container['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        return $c['renderer']->render($response->withStatus(404), 'errors/404.html');
    };
};