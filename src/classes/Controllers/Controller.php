<?php
    namespace Blog\Controllers;

    class Controller {

        protected $container;
        protected $model;

        protected function wrapView($response, $view, $args) {
            $this->container->get('renderer')->render($response, 'partials/header.phtml');
            $this->container->get('renderer')->render($response, $view, $args);
            $this->container->get('renderer')->render($response, 'partials/footer.phtml');
        }
    }