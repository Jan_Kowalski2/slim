<?php
    namespace Blog\Controllers;

    class PostController extends Controller {

        public function __construct($container) {
            $this->container = $container;
            $this->model = new \Blog\Models\PostModel($this->container->get('queryBuilder'));
            $this->model->init();
        }

        public function index($request, $response, $args) {

            $args['posts'] = $this->model->index();

            $this->wrapView($response, 'posts.phtml', $args);
        }

        public function show($request, $response, $args) {

            $resp = $this->model->show($args['id']);

            // TODO: handle invalid ID
            if (!empty($resp)) {
                $args = $resp[0];
                $this->wrapView($response, 'post.phtml', $args);
            }
        }

    }