<?php

    namespace Blog\Database;

    use PDO;

    class QueryBuilder {

        private $pdo;

        public function __construct(PDO $pdo) {
            $this->pdo = $pdo;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

        public function query($query) {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }

        public function selectAll($table) {
            return $this->query("SELECT * FROM {$table}");
        }
    }