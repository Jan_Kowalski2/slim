<?php
    namespace Blog\Models;

    use Interfaces\ModelInterface;

    class PostModel implements ModelInterface {

        private $keys = array(
            'id',
            'title',
            'content'
        );
        private $tableName = 'posts';
        private $queryBuilder;

        public function __construct($queryBuilder) {
            $this->queryBuilder = $queryBuilder;
        }

        public function init() {
            $this->queryBuilder->query("CREATE TABLE IF NOT EXISTS {$this->tableName} (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, content TEXT)");
        }

        public function index() {
            return $this->queryBuilder->selectAll($this->tableName);
        }

        public function show($id) {
            return $this->queryBuilder->query("SELECT * FROM {$this->tableName} WHERE id = {$id}");
        }

        public function getKeys() {
            return $this->keys;
        }

    }
